import multiprocessing
import paho.mqtt.publish as publish
import RPi.GPIO as GPIO
import os
import serial
from serial.tools import list_ports
import time as t
import requests
from datetime import datetime
import json
import paho.mqtt.client as mqtt
from subprocess import check_output


ip=0
while ip==0:
	try:
		ip = (int(((str(check_output(['hostname', '-I']))).split('.'))[3]))-100
	except:
		pass



ser = serial.Serial("/dev/ttyS0", 9600)     #SERIAL en GPIO

#VARIABLES CON EL NUMERO DEL GPIO EN DONDE ESTA CONECTADO EL RGB
red = 2
green = 22
blue = 3
GPIO.setmode(GPIO.BCM)
GPIO.setup (red, GPIO.OUT)
GPIO.setup (green,GPIO.OUT)
GPIO.setup (blue,GPIO.OUT)

#-----------------------------INICIAMOS LAS VARIABLES QUE USAREMOS--------------
station = ip
worker = 0
repeticion=0
cliente =str(ip)


###########################<<<<<FUNCIONES>>>>>########################

#------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#FUNCION PARPADEO DE LED VERDE PARA VERIFICAR QUE SE SUBIO LA CAJA
def verifica():
        GPIO.output(red, GPIO.HIGH)
        GPIO.output(green, GPIO.HIGH)
        GPIO.output(blue, GPIO.HIGH)


def caja_subida():
        for x in range (0,10):
                GPIO.output(red, GPIO.HIGH)
                GPIO.output(green, GPIO.HIGH)
                GPIO.output(blue, GPIO.HIGH)
                t.sleep(.1)
                GPIO.output(red, GPIO.LOW)
                GPIO.output(blue, GPIO.LOW)
                GPIO.output(green, GPIO.HIGH)
                t.sleep(.1)
def azul():
        GPIO.output(blue, GPIO.HIGH)
        GPIO.output(green, GPIO.LOW)
        GPIO.output(red, GPIO.LOW)

#FUNCION PARA CONTROLAR EL LED ROJO
def rojo():
        GPIO.output(red, GPIO.HIGH)
        GPIO.output(green, GPIO.LOW)
        GPIO.output(blue, GPIO.LOW)

#FUNCION PARA CONTROLAR EL LED VERDE
def verde():
        GPIO.output(red, GPIO.LOW)
        GPIO.output(green, GPIO.HIGH)
        GPIO.output(blue, GPIO.LOW)

#FUNCION PARA HACER EL COLOR AMARILL0 EN EL RGB
def amarillo():
        GPIO.output(red, GPIO.HIGH)
        GPIO.output(green, GPIO.HIGH)
        GPIO.output(blue, GPIO.LOW)


#FUNCION PARA HACER LA VERIFICACION DE CAMBIO DE EMPLEADO
def magenta():
        global repeticion
        while repeticion!=2:
                GPIO.output(red, GPIO.HIGH)
                GPIO.output(blue, GPIO.HIGH)
                GPIO.output(green, GPIO.LOW)
                t.sleep(.25) #PARPADEO DE .25 SEGUNDOS
                #PARPADEO EN COLOR BLANCO
                GPIO.output(red, GPIO.HIGH)
                GPIO.output(blue, GPIO.HIGH)
                GPIO.output(green, GPIO.HIGH)
                t.sleep(.15) #PARPADEO DE .15 SEGUNDOS
                repeticion=repeticion+1 #CONTEO DE EL NUMERO DE VUELTA
        #SETEAMOS EL RGB EN OFF OSEA SE APAGA RGB
        GPIO.output(red, GPIO.LOW)
        GPIO.output(blue, GPIO.LOW)
        GPIO.output(green, GPIO.LOW)
#------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

# FUNCION PARA CAMBIAR LA CAJA
def stop():
        print ("cambia de caja")
        flag=0
        while (flag == 0):
                t.sleep(.8)
                compare=PESO.value
                if compare>6:
                        pass
                if compare<=6:
                        y=datetime.now()
                        while (True):
                                compare2=PESO.value
                                if compare2>=6:
                                        y=datetime.now()
                                        break
                                if compare2<=6:
                                        dtt=datetime.now() - y
                                        five_seconds = (dtt.days * 24 *60 *60 +dtt.seconds)* 1000 +dtt.microseconds/1000.0
                                        if five_seconds>=500:
                                                print ("listo")
                                                flag=1
                                                break

#FUNCION PARA SUBIR LAS CAJAS MEDIANTE PROTOCOLO MQTT
def upload(peso):
        broker="192.168.100.24"
        port=1883
        topic = "upload"
        def on_publish(client,userdata,mid):
                print (mid)
                if mid==1:
                        print ("caja subida")
                        mid=0
        try:
                time = datetime.isoformat(datetime.utcnow())
                num = str(station) + time[5:7] + time[8:10] + time[11:13] + time[14:16] + time[17:19]
                weight=peso

                time = str(time[:-3]) + "Z"  #******************************************
                data = json.dumps({"id":int(num), "stationid": station, "time": time, "weight": weight})

                client1= mqtt.Client("basculas_" + str(station))        #SE LE DEBE DAR UN client_id para poder usar el QoS > 0
                client1.on_publish = on_publish                         #assign callback
                client1.connect(broker,port) #                          #establish connection

		        client1.loop_start()                                    #Iniciar LOOP() para poder publicar con QoS
                ret= client1.publish(topic, data, 2)                    #Publish con QoS = 2
                client1.loop_stop()                                     #Detener LOOP()


                ret=str(ret)                                            #Respuesta al PUBLISH
                if ret=="(0, 1)":
                    with open("database.txt","r") as f:
                        for line in f:
                            print (line)
                            ret= client1.publish(topic, line[:-1])
                        os.remove("database.txt")
                    f=open("database.txt", "a+")
                    f.close
                    t.sleep(1)

        except:
            print ("lost connection")
            f = open ("database.txt","a+")
            f.write(str(data)+"\n")
            f.close()

#-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
# FUNCION PARA COMPARAR EL PESO RECIBIDO CON LOS RANGOS Y SUBIR LA CAJA
def registro(PESO, condicion, pesoUnidad, pesoCaja, MxCaja, MnCaja, MxUnidad, MnUnidad):
        while True:
                maxx=MxCaja.value
                minn=MnCaja.value
                ma=MxUnidad.value
                mi=MnUnidad.value
                cond=int(condicion.value)
                if cond==1:
                        try:
                                peso=PESO.value
                                if (mi<=peso<=ma):
					pass
                                        #print (peso)
                                if (minn<=peso<=maxx):
                                        #print (peso)
                                        DONE.value=1
                                        upload(peso)
                                        stop()
                        except:
                                pass
############
############

#FUNCION CONTROL DE BACKLIGTH
def RGB(MxUnidad, MnUnidad, MxCaja, MnCaja, PESO, DONE):
        while True:
                Max_uni=MxUnidad.value
                Min_uni=MnUnidad.value
                Max_caja=MxCaja.value
                Min_caja=MnCaja.value
                valor=PESO.value
                done=DONE.value

                if (done!=1):
                        #--------- CONDICIONALES PARA INDICAR RANGO DE UNIDAD
                        if (valor<.10):
                                verifica()
                        if (.10<valor<Min_uni):
                                amarillo()
                        if (Min_uni<=valor<=Max_uni):
                                verde()
                        if (Max_uni<valor<=6):
                                rojo()
                        #----- CONDICIONALES PARA INDICAR RANGO DE CAJA
                        if (6<valor<Min_caja):
                                amarillo()
                        if (Min_caja<=valor<=Max_caja):
                                verde()
                        if (Max_caja<valor):
                                rojo()
                if (done==1):
                        caja_subida()
                        DONE.value=0

############
############
#FUNCION PARA LEER EL PESO ENVIADO POR LA BASCULA
def bascula(PESO):
        pes=0.0
        ser.reset_input_buffer()
        while True:
                try:
                        if ser.inWaiting()>0:
                                if (ser.read()==b'S'):
                                        try:
                                                msg = ser.read_until(' ')
                                                #print (msg)
                                                peso_raw=msg[2:-1]
                                                #status= msg[0]
                                                condicion.value=1
                                                PESO.value=float(peso_raw)
                                                #print (status)
                                        except:
			                        pass
                                if (ser.read()==b'D'):
                                        try:
                                                msg = ser.read_until(' ')
                                                #print (msg)
                                                peso_raw=msg[2:-1]
                                                #status= msg[0]
                                                condicion.value=2
                                                PESO.value=float(peso_raw)
                                                #print (status)
                                        except:
			                        pass
                except:
                        print ("falle en conversion")
                        ser.reset_input_buffer()
                        t.sleep(.1)

############
############
# FUNCION PARA RANGOS DE LA CAJA
def rangos(MxCaja, MnCaja, MxUnidad, MnUnidad):
    while True:
        try:
                broker = "192.168.100.24"
                topico = "rangos"

                def on_connect(client, userdata, flags, rc):
                        print("Connected with result code "+str(rc))
                        client.subscribe(topico)
                def on_message(client, userdata, msg):
                        mensaje=str(msg.payload)
                        print (mensaje)
                        MxCaja.value = float(str((mensaje.split(','))[1]))
                        MnCaja.value = float(str((mensaje.split(','))[0]))
                        print (MxCaja.value)
                        print (MnCaja.value)
                        MxUnidad.value = float(str((mensaje.split(','))[3]))
                        MnUnidad.value = float(str((mensaje.split(','))[2]))
                        print (MxUnidad.value)
                        print (MnUnidad.value)

                client = mqtt.Client()
                client.on_connect = on_connect
                client.on_message = on_message
                client.connect(broker, 1883, 60)
                client.loop_forever()
        except:

            pass
#-------------------------------COMENZAMOS LOS PROGRAMAS THREAD----------------------------------------------------------------------------------------------


if __name__ == "__main__":

        PESO = multiprocessing.Value('d', 0.0)
        pesoCaja = multiprocessing.Value('d', 1000)
        pesoUnidad = multiprocessing.Value('d', 100)
        MxCaja = multiprocessing.Value('d', 20)
        MnCaja = multiprocessing.Value('d', 17)
        MxUnidad = multiprocessing.Value('d', 3.25)
        MnUnidad = multiprocessing.Value('d', 3.150)
        DONE = multiprocessing.Value('d', 0)
        condicion = multiprocessing.Value('d', 0.0)

        p1 = multiprocessing.Process(target=registro, args=(PESO, condicion, pesoUnidad, pesoCaja, MxCaja, MnCaja, MxUnidad, MnUnidad))
        p2 = multiprocessing.Process(target=RGB, args=(MxUnidad, MnUnidad, MxCaja, MnCaja, PESO, DONE))
        p3 = multiprocessing.Process(target=bascula, args=(PESO, ))
        p4 = multiprocessing.Process(target=rangos, args=(MxCaja, MnCaja, MxUnidad, MnUnidad))


        p1.start()
        p2.start()
        p3.start()
        p4.start()

